class Machine {
    constructor () {
        this.enabled = false;
    }
    turnOn () {
        this.enabled = true;
        console.log('Turn on');
    }

    turnOff () {
        this.enabled = false;
        console.log('Turn off');
    }
}

class HomeAppliance extends  Machine {
    constructor () {
        super ();
        this.connected = false;
    }

    plugIn () {
        this.connected = true;
        console.log('Connected to the electricity.');
    }

    plugOff () {
        this.connected = false;
        console.log('Disconnected from the electricity.');
    }
}

class WashingMachine extends HomeAppliance {
    constructor () {
        super ();
    }

    run ()  {
        let self = this;
        if (!self.connected || !self.enabled) {
            console.log('You have to connect to the electricity.');
        } else {
            console.log('The washing machine is running.')
        }
    }
}

class LightSource extends HomeAppliance {
    constructor () {
        super ();
        this.levelE = 0;
    }

    setLevel (level) {
        if (level < 1 || level > 100) {
            console.log('Invalid level.');
        } else {
            this.levelE = level;
            console.log(`Light source ${level} watt.`);
        }
    }
}

class AutoVehicle extends Machine {
    constructor () {
        super ();
        this.x = 0;
        this.y = 0;
    }

    setPosition (x, y) {
        this.x = x;
        this.y = y;
        console.log(`Position of car ${x} ${y}`)
    }
}

class Car extends AutoVehicle {
    constructor () {
        super ();
        this.speed = 10;
    }

    setSpeed (speed) {
        this.speed = speed;
        console.log(`Speed of car ${speed}`);
    }

    run (x, y) {
        let interval = setInterval(() => {
            let newX = this.x + this.speed;
            let newY = this.y + this.speed;
            if (newX > x) newX = x;
            if (newY > y) newY = y;
            this.setPosition(newX, newY);
            if (newX === x && newY === y) clearInterval(interval);
        }, 1000);
    }
}

